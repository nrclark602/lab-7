/**************************
 *Nicholas Clark
 *CPSC 1021, 001, F20
 *nrclark@clemson.edu
 *Nushrat Humaira
 **************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs,const employee& rhs);
int myrandom (int i) { return rand()%i;}

int main(int argc, char const *argv[]) {   
	/*This is to seed the random generator */  
	srand(unsigned (time(0)));  

	/*Create an array of 10 employees and fill information from standard input with prompt messages*/ 
	employee employees[10]; 
	for (int i = 0; i < 10; ++i) {
		cout << "Enter last name, first name, birth year, and hourly wage for new employee: \n";
		cout << "Employee " << i+1 << ":\n";
		cin >> employees[i].lastName >> employees[i].firstName >> employees[i].birthYear >> employees[i].hourlyWage;
	}

	/*After the array is created and initialzed we call random_shuffle() see the   *notes to determine the parameters to pass in.*/   
	random_shuffle (begin(employees), end(employees), myrandom);

	/*Build a smaller array of 5 employees from the first five cards of the array created    *above*/    
	employee arr[5];
	for (int i = 0; i < 5; ++i) {
		arr[i] = employees[i];
	}
	/*Sort the new array.  Links to how to call this function is in the specs     *provided*/    
	sort(begin(arr), end(arr), name_order);

	/*Now print the array below */  
	for (int i = 0; i < 5; ++i) {
		cout << setw(5) << right << arr[i].lastName + ", " + arr[i].firstName << "\n";
		cout << setw(5) << right << arr[i].birthYear << "\n"; 
		cout << setw(5) << right << fixed << showpoint << setprecision(2) << arr[i].hourlyWage << "\n\n";
	}

	return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
	if (lhs.lastName < rhs.lastName) {
		return true;
	}
	else {
		return false;
	}

}





